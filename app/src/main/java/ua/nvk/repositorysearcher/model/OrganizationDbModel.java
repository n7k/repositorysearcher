package ua.nvk.repositorysearcher.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class OrganizationDbModel extends RealmObject {
    @PrimaryKey
    private String login;
    private int id;
    private String repos;
    private String avatar;
    private String name;
    private String blog;
    private String location;

    public OrganizationDbModel() {
        login = null;
        id = 0;
        repos = null;
        avatar = null;
        name = null;
        blog = null;
        location = null;
    }

    public OrganizationDbModel(String login, int id, String repos, String avatar, String name, String blog, String location) {
        this.login = login;
        this.id = id;
        this.repos = repos;
        this.avatar = avatar;
        this.name = name;
        this.blog = blog;
        this.location = location;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRepos() {
        return repos;
    }

    public void setRepos(String repos) {
        this.repos = repos;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrganizationDbModel that = (OrganizationDbModel) o;

        if (id != that.id) return false;
        if (!login.equals(that.login)) return false;
        if (repos != null ? !repos.equals(that.repos) : that.repos != null) return false;
        if (!avatar.equals(that.avatar)) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (blog != null ? !blog.equals(that.blog) : that.blog != null) return false;
        return location != null ? location.equals(that.location) : that.location == null;
    }

    @Override
    public int hashCode() {
        int result = login.hashCode();
        result = 31 * result + id;
        result = 31 * result + (repos != null ? repos.hashCode() : 0);
        result = 31 * result + avatar.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (blog != null ? blog.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OrganizationDbModel{" +
                "login='" + login + '\'' +
                ", id=" + id +
                ", repos='" + repos + '\'' +
                ", avatar='" + avatar + '\'' +
                ", name='" + name + '\'' +
                ", blog='" + blog + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
