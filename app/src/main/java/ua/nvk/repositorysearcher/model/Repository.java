package ua.nvk.repositorysearcher.model;

import com.google.gson.annotations.SerializedName;

public class Repository {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("html_url")
    private String htmlUrl;
    @SerializedName("description")
    private String description;

    public Repository() {
        id = 0;
        name = null;
        htmlUrl = null;
        description = null;
    }

    public Repository(int id, String name, String htmlUrl, String description) {
        this.id = id;
        this.name = name;
        this.htmlUrl = htmlUrl;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Repository{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", htmlUrl='" + htmlUrl + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
