package ua.nvk.repositorysearcher.model;

import io.realm.RealmObject;

public class RepositoryDbModel extends RealmObject {
    private int id;
    private String name;
    private String htmlUrl;
    private String description;

    public RepositoryDbModel() {
        id = 0;
        name = null;
        htmlUrl = null;
        description = null;
    }

    public RepositoryDbModel(int id, String name, String htmlUrl, String description) {
        this.id = id;
        this.name = name;
        this.htmlUrl = htmlUrl;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Repository{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", htmlUrl='" + htmlUrl + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
