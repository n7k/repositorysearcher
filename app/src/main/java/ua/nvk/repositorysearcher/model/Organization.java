package ua.nvk.repositorysearcher.model;

import com.google.gson.annotations.SerializedName;

public class Organization {
    @SerializedName("login")
    private String login;
    @SerializedName("id")
    private int id;
    @SerializedName("repos_url")
    private String repos;
    @SerializedName("avatar_url")
    private String avatar;
    @SerializedName("name")
    private String name;
    @SerializedName("blog")
    private String blog;
    @SerializedName("location")
    private String location;

    public Organization() {
        login = null;
        id = 0;
        repos = null;
        avatar = null;
        name = null;
        blog = null;
        location = null;
    }

    public Organization(String login, int id, String repos, String avatar, String name, String blog, String location) {
        this.login = login;
        this.id = id;
        this.repos = repos;
        this.avatar = avatar;
        this.name = name;
        this.blog = blog;
        this.location = location;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRepos() {
        return repos;
    }

    public void setRepos(String repos) {
        this.repos = repos;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "login='" + login + '\'' +
                ", id=" + id +
                ", repos='" + repos + '\'' +
                ", avatar='" + avatar + '\'' +
                ", name='" + name + '\'' +
                ", blog='" + blog + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
