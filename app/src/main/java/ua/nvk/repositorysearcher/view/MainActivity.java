package ua.nvk.repositorysearcher.view;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.nvk.repositorysearcher.R;
import ua.nvk.repositorysearcher.database.RealmDatabase;
import ua.nvk.repositorysearcher.model.Organization;
import ua.nvk.repositorysearcher.model.OrganizationDbModel;
import ua.nvk.repositorysearcher.presenter.LoadObserver;
import ua.nvk.repositorysearcher.presenter.MainActivityPresenter;
import ua.nvk.repositorysearcher.view.adapter.MainActivityRecyclerViewAdapter;
import ua.nvk.repositorysearcher.view.listener.RecyclerItemClickListener;

public class MainActivity extends AppCompatActivity implements MainActivityInterface {
    private static final int LAYOUT = R.layout.activity_main;

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.mainActivityRecyclerView)
    RecyclerView mainActivityRecyclerView;
    @BindView(R.id.orgProgressBar)
    ProgressBar orgProgressBar;
    @BindView(R.id.tvNoResults)
    TextView tvNoResults;
    @BindView(R.id.floatingActionMenu)
    FloatingActionMenu faMenu;
    @BindView(R.id.faMenuShowResults)
    FloatingActionButton faMenuShowResults;
    @BindView(R.id.faMenuClearDb)
    FloatingActionButton faMenuClearDb;

    private MainActivityPresenter mainActivityPresenter;
    private MainActivityRecyclerViewAdapter mainAdapter;
    private TextWatcher textWatcher;
    private Intent intent;
    private boolean isShowButtonActiveLast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        ButterKnife.bind(this);
        mainActivityPresenter = new MainActivityPresenter();
        intent = new Intent(this, RepositoryActivity.class);
        initSearch();
    }

    @Override
    public void initSearch() {
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mainAdapter.clearData();
                if (s.length() > 0 && s.length() % 3 == 0)
                    if (!isOnline())
                        Toast.makeText(getApplicationContext(), R.string.internet_connection_lost,
                                Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isShowButtonActiveLast = false;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 2) {
                    tvNoResults.setVisibility(View.GONE);
                    orgProgressBar.setVisibility(View.VISIBLE);
                    mainActivityPresenter.generateApiCall(s.toString(), new LoadObserver<Organization>() {
                        @Override
                        public void onDataLoaded(Organization organization) {
                            tvNoResults.setVisibility(View.GONE);
                            mainAdapter.setData(organization);
                            orgProgressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onDataLoadFailed(String message) {
                            if (message.contains(getApplication().getString(R.string.not_found))) {
                                orgProgressBar.setVisibility(View.GONE);
                                tvNoResults.setVisibility(View.VISIBLE);
                            } else {
                                tvNoResults.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), R.string.loading_failed,
                                        Toast.LENGTH_SHORT).show();
                                orgProgressBar.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            }
        };
        etSearch.addTextChangedListener(textWatcher);
        initRecyclerView();
    }

    @Override
    public void initRecyclerView() {
        mainAdapter = new MainActivityRecyclerViewAdapter();
        mainActivityRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mainActivityRecyclerView.setAdapter(mainAdapter);
        mainActivityRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                mainActivityRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                intent.putExtra("orgLogin", mainAdapter.getOrgLogin(position));
                intent.putExtra("orgName", mainAdapter.getOrgName(position));
                startActivity(intent);
            }
        }));
    }

    @Override
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    @Override
    public void actionMenuClose() {
        faMenuClearDb.hide(true);
        faMenuShowResults.hide(true);
        faMenu.close(true);
    }

    @OnClick
    public void clearDB(View v) {
        if (RealmDatabase.getInstance().hasData(OrganizationDbModel.class)) {
            RealmDatabase.getInstance().deleteData();
            mainAdapter.clearData();
            Toast.makeText(this, R.string.database_cleared, Toast.LENGTH_SHORT).show();
        }
        isShowButtonActiveLast = false;
        actionMenuClose();
    }

    @OnClick
    public void showLastSearchResults(View v) {
        if (!RealmDatabase.getInstance().hasData(OrganizationDbModel.class)) {
            Toast.makeText(this, R.string.database_is_empty, Toast.LENGTH_SHORT).show();
        } else {
            tvNoResults.setVisibility(View.GONE);
            mainAdapter.setData(mainActivityPresenter.reverseMap(RealmDatabase.getInstance()
                    .getData(OrganizationDbModel.class)));
        }
        isShowButtonActiveLast = true;
        actionMenuClose();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isShowButtonActiveLast", isShowButtonActiveLast);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (isShowButtonActiveLast = savedInstanceState.getBoolean("isShowButtonActiveLast"))
            mainAdapter.setData(mainActivityPresenter.reverseMap(RealmDatabase.getInstance()
                    .getData(OrganizationDbModel.class)));
    }

    @Override
    protected void onDestroy() {
        etSearch.removeTextChangedListener(textWatcher);
        super.onDestroy();
    }
}

