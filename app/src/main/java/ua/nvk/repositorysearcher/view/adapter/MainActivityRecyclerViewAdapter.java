package ua.nvk.repositorysearcher.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.nvk.repositorysearcher.R;
import ua.nvk.repositorysearcher.model.Organization;

public class MainActivityRecyclerViewAdapter extends RecyclerView.Adapter<MainActivityRecyclerViewAdapter.OrgViewHolder> {
    private List<Organization> organizationDataList = new ArrayList<>();

    public static class OrgViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivItemOrgAvatar)
        ImageView ivItemOrgAvatar;
        @BindView(R.id.tvItemOrgName)
        TextView tvItemOrgName;
        @BindView(R.id.tvItemOrgLocation)
        TextView tvItemOrgLocation;
        @BindView(R.id.tvItemOrgBlog)
        TextView tvItemOrgBlog;

        private OrgViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public OrgViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main_recyclerview_row, parent, false);
        return new OrgViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrgViewHolder holder, int position) {
        Glide.with(holder.ivItemOrgAvatar.getContext())
                .load(organizationDataList.get(position).getAvatar())
                .into(holder.ivItemOrgAvatar);
        holder.tvItemOrgName.setText(organizationDataList.get(position).getName());
        holder.tvItemOrgLocation.setText(organizationDataList.get(position).getLocation());
        holder.tvItemOrgBlog.setText(organizationDataList.get(position).getBlog());
    }

    @Override
    public int getItemCount() {
        return organizationDataList.size();
    }

    public void setData(Organization organization) {
        organizationDataList.clear();
        organizationDataList.add(organization);
        notifyDataSetChanged();
    }

    public void setData(List<Organization> organizations) {
        organizationDataList.clear();
        organizationDataList.addAll(organizations);
        notifyDataSetChanged();
    }

    public String getOrgLogin(int position) {
        return organizationDataList.get(position).getLogin();
    }

    public String getOrgName(int position) {
        return organizationDataList.get(position).getName();
    }

    public void clearData() {
        organizationDataList.clear();
        notifyDataSetChanged();
    }
}
