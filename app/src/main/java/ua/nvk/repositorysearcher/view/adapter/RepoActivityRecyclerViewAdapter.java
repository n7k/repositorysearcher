package ua.nvk.repositorysearcher.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.nvk.repositorysearcher.R;
import ua.nvk.repositorysearcher.model.Repository;

public class RepoActivityRecyclerViewAdapter extends RecyclerView.Adapter<RepoActivityRecyclerViewAdapter.RepoViewHolder> {
    private List<Repository> repositoryList = new ArrayList<>();

    public class RepoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvRepoItemOrgName)
        TextView tvRepoItemOrgName;
        @BindView(R.id.tvRepoItemDescription)
        TextView tvRepoItemDescription;

        private RepoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public RepoActivityRecyclerViewAdapter.RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_repository_recyclerview_row, parent, false);
        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoActivityRecyclerViewAdapter.RepoViewHolder holder, int position) {
        holder.tvRepoItemOrgName.setText(repositoryList.get(position).getName());
        if (repositoryList.get(position).getDescription() != null
                && repositoryList.get(position).getDescription().length() < 700)
            holder.tvRepoItemDescription.setText(repositoryList.get(position).getDescription());
        else holder.tvRepoItemDescription.setText(R.string.no_description);
    }

    @Override
    public int getItemCount() {
        return repositoryList.size();
    }

    public void setData(List<Repository> repository) {
        repositoryList.clear();
        repositoryList.addAll(repository);
        notifyDataSetChanged();
    }
}
