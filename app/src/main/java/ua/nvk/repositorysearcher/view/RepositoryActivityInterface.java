package ua.nvk.repositorysearcher.view;

public interface RepositoryActivityInterface {
    void initToolBar();

    void initRecyclerView();
}
