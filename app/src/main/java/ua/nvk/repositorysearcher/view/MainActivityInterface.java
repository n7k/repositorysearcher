package ua.nvk.repositorysearcher.view;

public interface MainActivityInterface {
    void initSearch();

    void initRecyclerView();

    boolean isOnline();

    void actionMenuClose();
}
