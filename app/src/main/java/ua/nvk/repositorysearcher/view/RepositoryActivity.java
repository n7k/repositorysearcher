package ua.nvk.repositorysearcher.view;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.nvk.repositorysearcher.App;
import ua.nvk.repositorysearcher.R;
import ua.nvk.repositorysearcher.model.Repository;
import ua.nvk.repositorysearcher.presenter.LoadObserver;
import ua.nvk.repositorysearcher.presenter.RepoActivityPresenter;
import ua.nvk.repositorysearcher.view.adapter.RepoActivityRecyclerViewAdapter;

public class RepositoryActivity extends AppCompatActivity implements RepositoryActivityInterface {
    private static final int LAYOUT = R.layout.activity_repository;

    @BindView(R.id.repoActivityRecyclerView)
    RecyclerView repoActivityRecyclerView;
    @BindView(R.id.repoProgressBar)
    ProgressBar repoProgressBar;

    private ActionBar toolbar;
    private RepoActivityPresenter repoActivityPresenter;
    private RepoActivityRecyclerViewAdapter repoAdapter;
    private Intent intent;
    private int reposCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        ButterKnife.bind(this);
        toolbar = getSupportActionBar();
        repoActivityPresenter = new RepoActivityPresenter();
        intent = getIntent();
        initToolBar();
        initRecyclerView();
    }

    @Override
    public void initToolBar() {
        toolbar.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(Html.fromHtml("<font color=\"#FFFFFF\">" +
                (repoActivityPresenter.orgNameCheck(intent.getStringExtra("orgName"))) + " " +
                App.getContext().getResources().getString(R.string.repositories) + "</font>"));
        toolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
    }

    @Override
    public void initRecyclerView() {
        repoProgressBar.setVisibility(View.VISIBLE);
        repoAdapter = new RepoActivityRecyclerViewAdapter();
        repoActivityRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        repoActivityRecyclerView.setAdapter(repoAdapter);
        repoActivityPresenter.generateApiCall(intent.getStringExtra("orgLogin"), new LoadObserver<List<Repository>>() {
            @Override
            public void onDataLoaded(List<Repository> repositoryList) {
                repoAdapter.setData(repositoryList);
                reposCount = repositoryList.size();
                toolbar.setTitle(Html.fromHtml("<font color=\"#FFFFFF\">" +
                        (repoActivityPresenter.orgNameCheck(intent.getStringExtra("orgName")))
                        + " " + App.getContext().getResources().getString(R.string.repositories) +
                        " (" + reposCount + ")" + "</font>"));
                repoProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onDataLoadFailed(String message) {
                Toast.makeText(getApplicationContext(), R.string.loading_failed,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
