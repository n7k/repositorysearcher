package ua.nvk.repositorysearcher.database;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import ua.nvk.repositorysearcher.model.OrganizationDbModel;

public class RealmDatabase implements RealmDbManipulator {
    private static RealmDatabase INSTANCE;
    private Realm realm;

    private RealmDatabase() {
        realm = Realm.getDefaultInstance();
    }

    public static RealmDatabase getInstance() {
        if (INSTANCE == null) INSTANCE = new RealmDatabase();
        return INSTANCE;
    }

    @Override
    public void saveData(OrganizationDbModel orgDbModel) {
        RealmResults<OrganizationDbModel> realmQuery = realm.where(OrganizationDbModel.class)
                .findAll();
        if (realmQuery.size() < 7) {
            realm.beginTransaction();
            realm.insertOrUpdate(orgDbModel);
            realm.commitTransaction();
        } else {
            realm.beginTransaction();
            realmQuery.deleteFirstFromRealm();
            realm.insertOrUpdate(orgDbModel);
            realm.commitTransaction();
        }
    }

    @Override
    public <T extends RealmObject> List<T> getData(Class<T> fileClass) {
        List<T> dataList = new ArrayList<>();
        RealmResults<T> realmQuery = realm.where(fileClass)
                .findAll();
        dataList.addAll(realmQuery);
        return dataList;
    }

    @Override
    public void deleteData() {
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }

    @Override
    public <T extends RealmObject> boolean hasData(Class<T> fileClass) {
        return !realm.where(fileClass).findAll().isEmpty();
    }

    @Override
    public void onRealmClose() {
        realm.close();
    }
}




