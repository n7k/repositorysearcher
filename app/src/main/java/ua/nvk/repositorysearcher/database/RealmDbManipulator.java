package ua.nvk.repositorysearcher.database;

import java.util.List;

import io.realm.RealmObject;
import ua.nvk.repositorysearcher.model.OrganizationDbModel;

public interface RealmDbManipulator {
    void saveData(OrganizationDbModel orgDbModel);

    <T extends RealmObject> List<T> getData(Class<T> fileClass);

    void deleteData();

    <T extends RealmObject> boolean hasData(Class<T> fileClass);

    void onRealmClose();
}
