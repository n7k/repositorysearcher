package ua.nvk.repositorysearcher.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ua.nvk.repositorysearcher.model.Organization;
import ua.nvk.repositorysearcher.model.Repository;

public interface Api {
    @GET("/orgs/{org}")
    Call<Organization> getOrganization(@Path("org") String org);

    @GET("/orgs/{org}/repos")
    Call<List<Repository>> getRepositories(@Path("org") String org);
}
