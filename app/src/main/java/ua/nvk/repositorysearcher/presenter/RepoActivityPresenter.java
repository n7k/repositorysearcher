package ua.nvk.repositorysearcher.presenter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nvk.repositorysearcher.App;
import ua.nvk.repositorysearcher.R;
import ua.nvk.repositorysearcher.model.Repository;
import ua.nvk.repositorysearcher.api.Api;
import ua.nvk.repositorysearcher.api.RetrofitProvider;

public class RepoActivityPresenter implements PresenterInterface {

    @Override
    public void generateApiCall(String args, final LoadObserver observer) {
        RetrofitProvider retrofitProvider = RetrofitProvider.getInstance();
        Api api = retrofitProvider.getApi();
        api.getRepositories(args).enqueue(new Callback<List<Repository>>() {
            @Override
            public void onResponse(Call<List<Repository>> call, Response<List<Repository>> response) {
                if (response.isSuccessful()) {
                    observer.onDataLoaded(response.body());
                }else {
                    observer.onDataLoadFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Repository>> call, Throwable t) {
                observer.onDataLoadFailed(t.getMessage());
            }
        });
    }

    public String orgNameCheck(String orgName) {
        if (orgName == null)
            return App.getContext().getResources().getString(R.string.no_name);
        else return orgName;
    }
}
