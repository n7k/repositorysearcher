package ua.nvk.repositorysearcher.presenter;

public interface PresenterInterface {
    void generateApiCall(String args, final LoadObserver observer);
}
