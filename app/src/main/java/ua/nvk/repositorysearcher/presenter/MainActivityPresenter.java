package ua.nvk.repositorysearcher.presenter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nvk.repositorysearcher.api.Api;
import ua.nvk.repositorysearcher.api.RetrofitProvider;
import ua.nvk.repositorysearcher.database.RealmDatabase;
import ua.nvk.repositorysearcher.mapper.OrganizationMapper;
import ua.nvk.repositorysearcher.model.Organization;
import ua.nvk.repositorysearcher.model.OrganizationDbModel;

public class MainActivityPresenter implements PresenterInterface {
    private OrganizationMapper orgMapper;

    public MainActivityPresenter() {
        orgMapper = new OrganizationMapper();
    }

    @Override
    public void generateApiCall(String args, final LoadObserver observer) {
        RetrofitProvider retrofitProvider = RetrofitProvider.getInstance();
        Api api = retrofitProvider.getApi();
        api.getOrganization(args).enqueue(new Callback<Organization>() {
            @Override
            public void onResponse(Call<Organization> call, Response<Organization> response) {
                if (response.isSuccessful()) {
                    observer.onDataLoaded(response.body());
                    RealmDatabase.getInstance().saveData(orgMapper.map(response.body()));
                } else observer.onDataLoadFailed(response.message());
            }

            @Override
            public void onFailure(Call<Organization> call, Throwable t) {
                observer.onDataLoadFailed(t.getMessage());
            }
        });
    }

    public List<Organization> reverseMap(List<OrganizationDbModel> orgDbModelList) {
        return orgMapper.reverseMap(orgDbModelList);
    }
}
