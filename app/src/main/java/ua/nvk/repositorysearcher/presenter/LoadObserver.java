package ua.nvk.repositorysearcher.presenter;

public interface LoadObserver<T> {
    void onDataLoaded(T obj);

    void onDataLoadFailed(String massage);
}
