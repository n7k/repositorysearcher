package ua.nvk.repositorysearcher.mapper;

import java.util.List;

import ua.nvk.repositorysearcher.model.Organization;
import ua.nvk.repositorysearcher.model.OrganizationDbModel;

public interface Mapper {
    OrganizationDbModel map(Organization organization);

    List<Organization> reverseMap(List<OrganizationDbModel> orgDbModelList);
}
