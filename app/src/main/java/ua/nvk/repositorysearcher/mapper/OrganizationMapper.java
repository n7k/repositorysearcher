package ua.nvk.repositorysearcher.mapper;

import java.util.ArrayList;
import java.util.List;

import ua.nvk.repositorysearcher.model.Organization;
import ua.nvk.repositorysearcher.model.OrganizationDbModel;

public class OrganizationMapper implements Mapper {

    @Override
    public OrganizationDbModel map(Organization org) {
        OrganizationDbModel model = new OrganizationDbModel();
        model.setLogin(org.getLogin());
        model.setId(org.getId());
        model.setRepos(org.getRepos());
        model.setAvatar(org.getAvatar());
        model.setName(org.getName());
        model.setBlog(org.getBlog());
        model.setLocation(org.getLocation());
        return model;
    }

    @Override
    public List<Organization> reverseMap(List<OrganizationDbModel> orgDbModelList) {
        List<Organization> organizationList = new ArrayList<>();
        for (OrganizationDbModel org : orgDbModelList) {
            Organization model = new Organization();
            model.setLogin(org.getLogin());
            model.setId(org.getId());
            model.setRepos(org.getRepos());
            model.setAvatar(org.getAvatar());
            model.setName(org.getName());
            model.setBlog(org.getBlog());
            model.setLocation(org.getLocation());
            organizationList.add(model);
        }
        return organizationList;
    }
}
